# Dot Files
This repository contains the dot files that can be applied to a machine
using [chezmoi](https://www.chezmoi.io/).


* Adding Files
```bash
chezmoi add .bashrc
```

* Editting Files
```bash
chezmoi edit .bashrc
```

* View and apply changes locally
```bash
chezmoi diff
chezmoi apply -v
```

* Commit changes
```bash
chezmoi cd
git status
```

* Setting up on a new machine
```bash
chezmoi init git@gitlab.msu.edu:schanzme/dotfiles.git
chezmoi diff
chezmoi apply
```

* Merge changes between local and repo
```bash
chezmoi merge .bashrc
```

* Pull latest changes from repository
```bash
chezmoi update -v
```
